# German translation for libgames-scores.
# Copyright (C) 2014 libgames-scores's COPYRIGHT HOLDER
# This file is distributed under the same license as the libgames-scores package.
# Daniel Raab <3dani33@gmail.com>, 2014.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2015-2016.
# Paul Seyfert <pseyfert@mathphys.fsk.uni-heidelberg.de>, 2016.
#
msgid ""
msgstr ""
"Project-Id-Version: libgames-scores master\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?"
"product=libgnome-games-support&keywords=I18N+L10N&component=general\n"
"POT-Creation-Date: 2016-11-19 15:42+0000\n"
"PO-Revision-Date: 2016-11-19 23:09+0100\n"
"Last-Translator: Paul Seyfert <pseyfert@mathphys.fsk.uni-heidelberg.de>\n"
"Language-Team: German <gnome-de@gnome.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 1.6.10\n"

#. Appears at the top of the dialog, as the heading of the dialog
#: games/scores/dialog.vala:58
msgid "Congratulations!"
msgstr "Herzlichen Glückwunsch!"

#: games/scores/dialog.vala:60
msgid "High Scores"
msgstr "Bestenliste"

#: games/scores/dialog.vala:62
msgid "Best Times"
msgstr "Beste Zeiten"

#: games/scores/dialog.vala:78
msgid "No scores yet"
msgstr "Noch keine Punktzahlen"

#: games/scores/dialog.vala:82
msgid "Play some games and your scores will show up here."
msgstr "Die in Ihren Spielen erreichten Punktzahlen werden hier angezeigt."

#. A column heading in the scores dialog
#: games/scores/dialog.vala:135
msgid "Rank"
msgstr "Platzierung"

#. A column heading in the scores dialog
#: games/scores/dialog.vala:144
msgid "Score"
msgstr "Punktzahl"

#: games/scores/dialog.vala:146
msgid "Time"
msgstr "Zeit"

#. A column heading in the scores dialog
#: games/scores/dialog.vala:153
msgid "Player"
msgstr "Spieler"

#. Appears on the top right corner of the dialog. Clicking the button closes the dialog.
#: games/scores/dialog.vala:163
msgid "_Done"
msgstr "_Fertig"

#. Time which may be displayed on a scores dialog.
#: games/scores/dialog.vala:278
#, c-format
msgid "%ld minute"
msgid_plural "%ld minutes"
msgstr[0] "%ld Minute"
msgstr[1] "%ld Minuten"

#. Time which may be displayed on a scores dialog.
#: games/scores/dialog.vala:280
#, c-format
msgid "%ld second"
msgid_plural "%ld seconds"
msgstr[0] "%ld Sekunde"
msgstr[1] "%ld Sekunden"

#: games/scores/dialog.vala:286
msgid "Your score is the best!"
msgstr "Sie haben die höchste Punktzahl!"

#: games/scores/dialog.vala:288
msgid "Your score has made the top ten."
msgstr "Sie sind unter den zehn Besten."
